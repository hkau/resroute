# resroute

Quick [Preact](https://preactjs.com) server that supports SSR. Uses [JellyFish Router](https://codeberg.org/hkau/jellyfish-router) as a base.

## Installation

Resroute requires the [Bun](https://bun.sh) runtime to run.

```
npm i resroute
bun a resroute
```

## Usage

1. Create a `.env` file, see the example `.env` file provided in the root of this repository for an easy example
2. Start server and define routes, see `example/index.test.ts` as an example

    - Routes are defined using the `RegisterRoute` function

        ```ts
        import { Serve, RegisterRoute } from "resroute";

        // register routes
        RegisterRoute("/", ["GET"]);
        ```

    - Routes must be registered before they will work

### `RegisterRoute`

All routes must be registered before they will serve content (besides all content under `/_static`)

`RegisterRoute` tells the server which routes you expect to work on your server.

```ts
function RegisterRoute(
    path: string,
    supportedMethods: Method[],
    supportOldRouting?: boolean,
    returnRoute: boolean
): void;
```

### Old Routing

Sometimes, routes such as `/blog/[slug]` might not work properly. You can tell a route to use old routing (routing from [JellyFish Router](https://codeberg.org/hkau/jellyfish-router)) if you want a simple catch-all route. When using old routing, all paths under a route will be directed to `:page.tsx`, the blog example directs all paths under `/blog` to the route `/blog/:page.tsx`. If there is an `index.tsx` file present and there is a separate route that does not support old routing on the same path, that will be used.

For example:

```ts
// example/index.test.ts
RegisterRoute("/blog", ["GET"]);
RegisterRoute("/blog", ["GET"], true);
```

If the user requested `/blog/test`, they would be given `/blog/:page.tsx`. If they requested `/blog`, they would be given `/blog/index.tsx`.

The `match.params` object will be replaced with an array containing every path after the old route name.

### Return Response

Setting `returnResponse` to `true` while registering your route allows you to return a response object instead of JSX.

For example:

```ts
// example/index.test.ts
RegisterRoute("/jsonreturn", ["GET"], true, true);

// example/jsonreturn.tsx
export default function JSONReturnTest() {
    return new Response(
        JSON.stringify({
            "Content-Type": "application/json; charset=utf-8",
        }),
        {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        }
    );
}
```

### Example Page

```tsx
// layout: /pages/blog/[slug].tsx
export default function Post(match: any) {
    return <h1>{match.params.slug}</h1>;
}
```

If using old routing, this would give the same result:

```tsx
// layout: /pages/blog/:page.tsx
export default function Post(match: any) {
    return <h1>{match.params[0]}</h1>;
}
```
