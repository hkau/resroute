/**
 * @file Handle routes
 * @name RouteHandler.ts
 * @license MIT
 */

import path from "node:path";
import fs from "node:fs";

// @ts-ignore
import { isText } from "istextorbinary";
import { contentType } from "mime-types";
import type { VNode } from "preact";

// using render from the awesome NPM module, preact-render-to-string
// because preact cannot render without a DOM normally (annoying)
import { render } from "preact-render-to-string";

// types
export type Method = "GET" | "POST" | "PUT" | "HEAD" | "DELETE" | "OPTIONS";
export type RouteListing = { p: string; m: Method[]; sr: boolean; rr: boolean };

let routes: { [key: string]: RouteListing } = {};

// route cache
const routeCache: { [key: string]: any } = {}; // cache route functions (transpiled tsx)
const staticCache: { [key: string]: any } = {}; // cache static files (text only)

// create router
let ROUTER_DIR = process.env.ROUTER_DIR || path.resolve(process.cwd(), "pages");

let ROUTER_PUBLIC =
    process.env.ROUTER_PUBLIC || path.resolve(process.cwd(), "public");

ROUTER_DIR = ROUTER_DIR.replace(":cwd", process.cwd());
ROUTER_PUBLIC = ROUTER_PUBLIC.replace(":cwd", process.cwd());

const Router = new Bun.FileSystemRouter({
    style: "nextjs",
    dir: ROUTER_DIR,
    origin: process.env.ROUTER_ORIGIN,
    // assetPrefix: "_static/", // we handle this ourselves below
});

// fill routeCache

// importing everything here instead of on every request fixes the slow load speeds
// and the random segfault that would occur when loading multiple pages quickly

for (let route of Object.entries(Router.routes)) {
    if (!route[1].endsWith(".tsx") && !route[1].endsWith(".jsx")) continue;
    const module = await import(route[1]);
    if (!module.default)
        throw new Error("Imported TSX module does not export a default function!");

    // the module is expected to return a default function
    routeCache[route[1]] = module.default; // cache the route function for future use
}

if (fs.existsSync(path.join(ROUTER_DIR, ":head.tsx"))) {
    const module = await import(path.join(ROUTER_DIR, ":head.tsx"));
    routeCache.head = module.default();
}

/**
 * @function HandleRoute
 * @description Attempt to run a route from a request
 *
 * @export
 * @param {Request} request
 * @return {Response}
 */
export async function HandleRoute(request: Request): Promise<Response> {
    const url = new URL(request.url);

    // if url.pathname starts with "_static", serve file straight from router.dir
    if (url.pathname.startsWith("/_static")) {
        const filePath = path.join(
            ROUTER_PUBLIC,
            url.pathname.replace("_static/", "")
        );

        // check if file exists
        if (!fs.existsSync(filePath))
            return new Response("404: Not Found!", { status: 404 });

        // if file is a text file, save to staticCache
        if (isText(path.basename(filePath)) && !staticCache[url.pathname])
            staticCache[url.pathname] = fs.readFileSync(filePath).toString();

        // get content
        const content: string | ArrayBuffer = isText(path.basename(filePath))
            ? staticCache[url.pathname]
            : await Bun.file(filePath).arrayBuffer();

        if (!content)
            throw new Error("Static file does not yet exist in the static cache!");

        // return file
        return new Response(content, {
            headers: {
                "Content-Type":
                    contentType(path.basename(filePath)) ||
                    "text/plain; charset=utf-8",
            },
        });
    }

    // match
    let match: any = Router.match(url.pathname);
    Bun.gc(false);

    // if not match, but routes[url.pathname] exists, create fake match
    const _fakeMatch = Object.entries(routes).find((x) => {
        return url.pathname.startsWith(x[1].p) && x[1].sr === true;
    });

    if (!match && _fakeMatch) {
        let params = url.pathname.replace(_fakeMatch[0], "").split("/");
        params.shift(); // remove empty space from beginning

        let filePath = path.join(ROUTER_DIR, _fakeMatch[1].p, ":page.tsx");
        if (
            url.pathname === _fakeMatch[1].p ||
            url.pathname === `${_fakeMatch[1].p}/` // trailing slash
        )
            // serve index.tsx instead if url.pathname matches route path
            filePath = path.join(ROUTER_DIR, _fakeMatch[1].p, "index.tsx");

        match = {
            name: _fakeMatch[1].p,
            filePath,
            params,
        };
    }

    // return 404 if match is invalid
    if (!match) return new Response("404: Not Found!", { status: 404 });

    // add request to match
    match.request = request;

    // check method
    const routeStore = routes[match.name];
    if (!routeStore) return new Response("404: Not Found!", { status: 404 });

    if (!routeStore.m.includes(request.method as Method))
        return new Response("405: Method Not Allowed!", { status: 405 });

    // get route from routeCache
    let route = routeCache[match.filePath];
    if (!route)
        throw new Error(
            "Route does not exist in cache! Try to restart the server to rescan routes."
        );

    // get :head.tsx content (if it exists)
    let head: VNode<{}> = {
        type: "div",
        props: {
            children: undefined,
        },
        key: undefined,
    };

    // load element head from routeCache
    if (routeCache.head) head = routeCache.head;

    // return
    // check for rr (return Response)
    if (!routeStore.rr)
        return new Response(
            `<!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    ${render(head)}
                </head>
                
                <body>
                    <div id="${
                        process.env.TITLE || "Server"
                    }.templates.body" style="display: contents">
                        ${render(await route(match))}
                    </div>
                </body>
            </html>`,
            {
                headers: {
                    "Content-Type": "text/html; charset=utf-8",
                },
            }
        );
    else return (await route(match)) as Response; // return Response (rr = true)
}

/**
 * @function RegisterRoute
 *
 * @export
 * @param {string} path
 * @param {Method[]} supportedMethods
 * @param {Route} routeFunction
 */
export function RegisterRoute(
    path: string,
    supportedMethods: Method[],
    supportOldRouting = false,
    returnRoute = false
) {
    // add route
    routes[path] = {
        p: path,
        m: supportedMethods,
        sr: supportOldRouting,
        rr: returnRoute,
    };
}

// default export
export default {
    HandleRoute,
    RegisterRoute,
};
