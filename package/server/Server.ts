/**
 * @file Handle server
 * @name Server.ts
 * @license MIT
 */

import { HandleRoute } from "./RouteHandler.js";
import { Server } from "bun";

/**
 * @function Serve
 *
 * @export
 * @param {number} [port=80]
 * @return {Server}
 */
export function Serve(
    port: number = 80
): Server {
    const server = Bun.serve({
        port,
        async fetch(request, server) {
            return await HandleRoute(request);
        },
    });

    // log
    console.log(
        `\x1b[92m[${process.env.TITLE || "Server"}]\x1b[0m`,
        `Started server on port :${port}`,
        `| http://localhost:${port}`
    );

    // return
    return server;
}

// default export
export default Serve;
