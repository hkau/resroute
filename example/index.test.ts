import { Serve, RegisterRoute } from "../package/index";

// register routes
RegisterRoute("/", ["GET"]);
RegisterRoute("/blog", ["GET"]);
RegisterRoute("/blog", ["GET"], true);
RegisterRoute("/jsonreturn", ["GET"], true, true);

// start server
Serve(5175);
