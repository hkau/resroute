// this page is an example of old routing,
// params are supplied through an array (string[]) instead of an object

export default function BlogPost(match: any) {
    return <h1>{match.params[0]}</h1>;
}
