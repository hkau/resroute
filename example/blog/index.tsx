export default function Blog() {
    return (
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
            temporibus aliquid soluta. Mollitia alias quo, velit similique officia
            voluptas suscipit odit quibusdam distinctio autem! Assumenda consectetur
            nostrum quo maiores possimus.
            <a href="/blog/example-post">View Example Post</a>
        </p>
    );
}
