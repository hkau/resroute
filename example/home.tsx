const rendered = new Date().toLocaleString();

export default function Home() {
    return (
        <div>
            <p>Rendered: {rendered}</p>
            <p><a href="/blog">Blog</a></p>
            <p><a href="/jsonreturn">JSON Return Test</a></p>
        </div>
    );
}
