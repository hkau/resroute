export default function JSONReturnTest() {
    return new Response(
        JSON.stringify({
            "Content-Type": "application/json; charset=utf-8",
        }),
        {
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        }
    );
}
